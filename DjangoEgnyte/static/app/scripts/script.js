'use strict';


var actionChart = dc.barChart('#action-chart');
var userChart = dc.rowChart('#user-chart');
var workgroupChart = dc.rowChart('#workgroup-chart');
var timeChart = dc.lineChart("#hourly-chart");
var spaceUsageAction = dc.barChart("#data-usage-action");

var actionSourceChart = dc.barChart('#action-soruce-chart');
var actionSourceAvgChart = dc.barChart('#data-usage-action-avg');

var spaceUsageTotal = dc.numberDisplay("#data-usage-total");
var actionTotal = dc.numberDisplay("#total-action-chart");
var userAgentChart = dc.rowChart('#user-agent-chart');



var dateFormat = d3.time.format('%m/%d/%Y');
var numberFormat = d3.format('.2f');
var dateFormat = d3.time.format.iso.parse;



$("#reset").click(function () {
    actionChart.filterAll();
    userChart.filterAll();
    workgroupChart.filterAll();
    timeChart.filterAll();
    spaceUsageAction.filterAll();
    actionSourceChart.filterAll();
    dc.redrawAll();
});


    d3.queue().defer(d3.json, "data")
        .await(charts);








function charts(error, data, data2, data3, data4) {


    data.forEach(function (d) {
        d.g = new Date(parseInt(d.g));
        d.c = parseInt(d.c);
        if (d.h == '')
            d.h = 'Agent unknown'
});

    var ndx = crossfilter(data);


   var all = ndx.groupAll();


  

   var actionDimension = ndx.dimension(function (d) {
    return d.a;
});

//* Number of rows per group	
    var actionGroup = actionDimension.group().reduceCount(function (d) { return d; });;

    var actionSpaceUsedGroup = actionDimension.group().reduceSum(function (d) { return d.c; });


    var userDimension = ndx.dimension(function (d) {
        return d.f;
});
//* Number of rows per user	
    var userGroup = userDimension.group().reduceCount(function (d) { return d; });

    var workgroupDimension = ndx.dimension(function (d) {
        return d.i;
    });

    var workgroupGroup = workgroupDimension.group().reduceCount(function (d) { return d; });

    var actionSourceDimension = ndx.dimension(function (d) {
        return d.b;
    });

    var actionSourceGroup = actionSourceDimension.group().reduceCount(function (d) { return d; });


    var userAgentDimension = ndx.dimension(function(d){
        return d.h;
    });

var volumeByHour = ndx.dimension(function (d) {
        return d3.time.hour(d.g);
    });

var volumeByHourGroup = volumeByHour.group()
    .reduceCount(function (d) { return d; });

var userAgentGroup = userAgentDimension.group().reduceCount(function (d) { return d; });


    var spaceUsedAccessor = function (d) { return d.c; };
var strmSpaceUsedExtent = [];
strmSpaceUsedExtent = d3.extent(data, spaceUsedAccessor);




var totalSpaceUsed = all.reduceSum(function (d) {
    return d.c;
});
var totalActions = all.reduceCount(function (d) { return d; });


var averageSpaceUsed = actionDimension.group().reduce(
    //add
    function (p, v) {
        p.count++;
        p.sum += v['c'];
        p.avg = d3.round((p.sum / p.count), 2);
        return p;
    },
    //remove
    function (p, v) {
        p.count--;
        p.sum -= v['c'];
        p.avg = d3.round((p.sum / p.count), 2);
        return p;
    },
    //init
    function (p, v) {
        return { count: 0, avg: 0, sum: 0 };
    });



    actionChart.width(1080)
        .height(550)
        .margins({ top: 10, right: 10, bottom: 150, left: 60 })
        .dimension(actionDimension)
        .group(actionGroup)
        .transitionDuration(500)
        .gap(5)  // 65 = norm
        //    .filter([3, 5])
        .x(d3.scale.ordinal())
        .xUnits(dc.units.ordinal)
        .brushOn(false)
        .elasticY(true)
        .renderlet(function (chart) {
            chart.selectAll("g.x text")
                .attr('transform', "translate(-30,45) rotate(-35)")
                ;
           
        })
    .xAxis().tickFormat();

    spaceUsageAction.width(1080)
        .height(550)
        .margins({ top: 10, right: 10, bottom: 140, left: 100 })
        .dimension(actionDimension)
        .group(actionSpaceUsedGroup)
        .transitionDuration(500)
        .gap(5)  // 65 = norm
        //    .filter([3, 5])
        .x(d3.scale.ordinal())
        .xUnits(dc.units.ordinal)
        .brushOn(false)
        .elasticY(true)
        .renderlet(function (chart) {
            chart.selectAll("g.x text")
                .attr('transform', "translate(-30,45) rotate(-35)")
        })
        .xAxis().tickFormat();

    actionSourceAvgChart.width(1080)
        .height(550)
        .margins({ top: 10, right: 10, bottom: 140, left: 100 })
        .dimension(actionDimension)
        .group(averageSpaceUsed)
        .transitionDuration(500)
        .gap(5)  // 65 = norm
        //    .filter([3, 5])
        .x(d3.scale.ordinal())
        .xUnits(dc.units.ordinal)
        .brushOn(false)
        .elasticY(true)
        .renderlet(function (chart) {
            chart.selectAll("g.x text")
                .attr('transform', "translate(-30,45) rotate(-35)")
        })
        .valueAccessor(function (d) {
            return d.value.avg;
        })
        .xAxis().tickFormat();


    actionSourceChart.width(1080)
        .height(550)
        .margins({ top: 10, right: 10, bottom: 150, left: 60 })
        .dimension(actionSourceDimension)
        .group(actionSourceGroup)
        .transitionDuration(500)
        .gap(5)  // 65 = norm
        //    .filter([3, 5])
        .x(d3.scale.ordinal())
        .xUnits(dc.units.ordinal)
        .brushOn(false)
        .elasticY(true)
        .renderlet(function (chart) {
            chart.selectAll("g.x text")
                .attr('transform', "translate(-30,45) rotate(-35)")
        })
        .xAxis().tickFormat();

    userChart.width(1000)
        .height(250)
        .margins({ top: 10, right: 10, bottom: 20, left: 60 })
        .dimension(userDimension)
        .group(userGroup)
        .elasticX(true)
        //.transitionDuration(500)
        //    .filter([3, 5])
        //.x(d3.scale.ordinal().domain(data.map(function (d) { return d.user_id; })))
        //.elasticY(true)
        //.renderlet(function (chart) {
        //    chart.selectAll("g.x text")
        //        .attr('transform', "rotate(-90)");
        //})
        //.xAxis().tickFormat();
        ;

    userChart.data(function (group) {
        return group.top(10);
    });

    workgroupChart.width(1000)
        .height(250)
        .margins({ top: 10, right: 10, bottom: 20, left: 60 })
        .dimension(workgroupDimension)
        .group(workgroupGroup)
        .elasticX(true)

        ;
    workgroupChart.data(function (group) {
        return group.top(10);
    });

    userAgentChart.width(1000)
        .height(250)
        .margins({ top: 10, right: 10, bottom: 20, left: 60 })
        .dimension(userAgentDimension)
        .group(userAgentGroup)
        .elasticX(true)

        ;
    userAgentChart.data(function (group) {
        return group.top(10);
    });

    // time graph
    timeChart.width(1080)
        .height(550)
        .transitionDuration(500)
        //    .mouseZoomable(true)
        .margins({ top: 10, right: 10, bottom: 20, left: 60 })
        .dimension(volumeByHour)
        .group(volumeByHourGroup)
        .elasticY(true)

        .x(d3.time.scale().domain(d3.extent(data,
            function (d)
            {
                return d3.time.hour(d.g);;
            })))
        .xAxis();

    spaceUsageTotal
        .group(totalSpaceUsed)
        .valueAccessor(function (d) {
            return d.sum;
        })
        .formatNumber(d3.format(".3s"));

    actionTotal
        .valueAccessor(function (d) {
            return d;
        })
        .group(totalActions)
        .formatNumber(d3.format(".3"));

    
dc.renderAll();
}