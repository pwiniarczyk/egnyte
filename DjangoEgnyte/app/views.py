"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
import json
from django.http import HttpResponse
from django.http import JsonResponse
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
import MySQLdb

cache_opts = {
    'cache.type': 'file',
    'cache.data_dir': '/tmp/cache/data',
    'cache.lock_dir': '/tmp/cache/lock'
}

cache = CacheManager(**parse_cache_config_options(cache_opts))
add = 1

cache.get_cache('data').clear()


query = "SELECT action as a, action_source as b, space_used as c, target_storage_time as d, user_id as f, time_stamp as g, LEFT(user_agent, LOCATE('(',user_agent)-2) as h, work_group_id as i FROM egnyte.egnyte limit 10000;"



@cache.cache('data')
def performQuery(limit):
    # do something to retrieve data
    conn=MySQLdb.connect(host="localhost",user="root",passwd="root",db="egnyte")
    cursor = conn.cursor()
    query = ("SELECT action as a, action_source as b, space_used as c,  user_id as f, time_stamp as g, LEFT(user_agent, LOCATE('(',user_agent)-2) as h, work_group_id as i FROM egnyte.egnyte order by time_stamp limit " + str(limit) +";")
    cursor.execute(query)
    fields = [d[0] for d in cursor.description]
    result = [dict(zip(fields,row)) for row in cursor.fetchall()]
    cursor.close()
    conn.close()
    return json.dumps(result)


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def data(request):
    return HttpResponse(performQuery(1500000))